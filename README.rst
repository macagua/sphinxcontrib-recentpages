=========================================================
sphinxcontrib-recentpages:  Sphinx Recent Pages Extension
=========================================================

This sphinx extension provides a new directive which displays recent updated files list.

.. contents::
   :depth: 2


Introduction
============

This sphinx extension provides a new directive which displays recent updated files list.
If you want to get all page list ordered by mtime, just put recentpages directive as follows:

::

  .. recentpages::


If you want to display recent 3 files only, add num option to the directive:
  
::

  .. recentpages::
      :num: 3

  

Install
=======


Use ``easy_install``::

  easy_install sphinxcontrib-recentpages

Then add the following line into source/conf.py::

  extensions += ['sphinxcontrib.recentpages']

Recentpages on sidebar
----------------------

Recentpages can be displayed on sidebar.

First, please enable recentpages_sidebar in source/conf.py::

  recentpages_sidebar = True

After you enable this, recentpages extension will create a template file 'recentpages.html' in your template path ( '_templates' by default ).

Please make sure 'template_path' property contains at least 1 writable directory if you customize 'template_path'.

Then add 'recentpages.html' to 'html_sidebars' dictionary. For example, add the following line to source/conf.py if you want to add under the default sidebar::

  html_sidebars = {'**': ['localtoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html', 'recentpages.html']}

By default, 5 pages are displayed on sidebar.
If you want to change the number of pages on the list, please change 'recentpages_sidbar_pages' property::

  recentpages_sidebar_pages = 10



